# Generated by Django 4.1.7 on 2023-04-23 17:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('productos', '0011_pedido_nombre_cliente'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pedido',
            name='fecha_pedido',
        ),
        migrations.AddField(
            model_name='pedido',
            name='fecha_fin_pedido',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='pedido',
            name='fecha_inicio_pedido',
            field=models.DateTimeField(null=True),
        ),
    ]
